//
//  Person.swift
//  SwiftDemo_PL2
//
//  Created by Catarina Silva on 09/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import Foundation

class Person: NSObject, NSCoding {
    var firstName:String?
    var lastName:String?
    //var nationality = "Portuguese"
    var nationality:String
    
    override init () {
        self.nationality = "Portuguese"
    }
    
    init (firstName:String, lastName: String, nationality:String){
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    struct Consts {
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let nationality = "nationality"
    }
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(firstName, forKey: Consts.firstName)
        aCoder.encodeObject(lastName, forKey: "lastName")
        aCoder.encodeObject(nationality, forKey:"nationality")
    }
    
    required init?(coder aDecoder: NSCoder) {

        firstName = aDecoder.decodeObjectForKey(Consts.firstName) as? String ?? ""
        lastName = aDecoder.decodeObjectForKey(Consts.lastName) as? String ?? ""
        nationality = (aDecoder.decodeObjectForKey(Consts.nationality) as? String) ?? ""
        
    }
    
    func fullName() -> String {
//        var auxName:[String] = [ ]
//        
//        if let firstName = self.firstName {
//            auxName += [firstName]
//        }
//        if let lastName = self.lastName {
//            auxName += [lastName]
//        }
//        
//        return auxName.joinWithSeparator(" ")
        
        return firstName! + " " + lastName!
    }
}

