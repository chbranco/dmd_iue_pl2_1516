//
//  ArchivePerson.swift
//  SwiftDemo_PL2
//
//  Created by Catarina Silva on 26/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import Foundation


class ArchivePerson {
    var documentsPath: NSURL = NSURL()
    var filePath:NSURL = NSURL()
    
    func savePerson(p: Person){
        documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0])
        print("documentsPath: \(documentsPath)")
        
        filePath = documentsPath.URLByAppendingPathComponent("Person.data")
        print("filepath: \(filePath)")
        
        let path = filePath.path!
        
        if NSKeyedArchiver.archiveRootObject(p, toFile: path) {
            print("Success")
        } else {
            print("Failure")
        }
    }
    
    func retrievePerson() -> Person {
        var dataToRetrieve = Person()
        
        documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0])
        filePath = documentsPath.URLByAppendingPathComponent("Person.data", isDirectory: false)
        
        let path = filePath.path!
        
        if let newData = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? Person {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
}