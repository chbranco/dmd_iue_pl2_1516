//
//  ViewController.swift
//  SwiftDemo_PL2
//
//  Created by Catarina Silva on 05/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import UIKit
import CoreLocation
import Social

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var firstNameField: UITextField!
    
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var nationalityField: UITextField!
    
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    let person = Person()
    
    let defaults = NSUserDefaults()
    
    let locationManager = CLLocationManager()
    
    var currentLocation = CLLocationCoordinate2D()
    
    
    let nonChangeableVar = "Hi";
    var changeableVar:String = "By";
    var name : String? ;
    
    struct imcMeasures {
        var height:Double = 0.0
        var weight:Double = 0.0
    }
    
    var johnDoeMeasures = imcMeasures(height: 1.85, weight: 90.5)
    let janeDoeMeasures = imcMeasures(height: 1.65, weight: 55.5)

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("New location!!")
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        
        if let c = coord {
            print("lat: \(c.latitude) long: \(c.longitude))")
            currentLocation = c
            defaults.setDouble(currentLocation.latitude, forKey: "latitude")
            defaults.setDouble(currentLocation.longitude, forKey: "longitude")
        }
    }

    
    @IBAction func shareAction(sender: UIBarButtonItem) {
        
        let actionSheet = UIAlertController(title: "", message: "Share your location", preferredStyle: UIAlertControllerStyle.ActionSheet)
        let tweetAction = UIAlertAction(title: "Share on Twitter", style: UIAlertActionStyle.Default) {(action) -> Void in
            //code the tweet post
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
                
                let twitterComposeVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                
                twitterComposeVC.setInitialText("My current location is \(self.currentLocation.latitude), \(self.currentLocation.longitude)")
                self.presentViewController(twitterComposeVC, animated: true, completion: nil)
                
                
                
            } else {
                self.showAlertMessage("You must first log on to your Twitter account");
            }

        }
        let facebookAction = UIAlertAction(title: "Share on Facebook", style: UIAlertActionStyle.Default) {(action) -> Void in
            //code the FB post
            
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
                let facebookComposeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                
                facebookComposeVC.setInitialText("My current location is \(self.currentLocation.latitude), \(self.currentLocation.longitude)")
                
                self.presentViewController(facebookComposeVC, animated: true, completion: nil)
                
            } else {
              self.showAlertMessage("You must first log on to your facebook account");
            }
        }
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) {(action) -> Void in

        }
        actionSheet.addAction(tweetAction)
        actionSheet.addAction(facebookAction)
        actionSheet.addAction(dismissAction)
        presentViewController(actionSheet, animated: true, completion: nil)
    

        
    }
    
    func showAlertMessage (message:String) {
        let alertController = UIAlertController(title: "Error sharing", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func savePerson(sender: UIButton) {
        //should protect empty textFields!!!
        let p = Person(firstName: firstNameField.text ?? "", lastName: lastNameField.text ?? "", nationality: nationalityField.text ?? "")
    
        ArchivePerson().savePerson(p)
    }
    
    
    
    
    @IBAction func getPerson(sender: UIButton) {
        let p = ArchivePerson().retrievePerson()
        
        firstNameField.text = p.firstName
        lastNameField.text = p.lastName
        nationalityField.text = p.nationality
    }
    
    
    @IBAction func getLocation(sender: UIButton) {
        
        
        locationManager.startUpdatingLocation()
        
        latitudeLabel.text = "\(currentLocation.latitude)"
        longitudeLabel.text = "\(currentLocation.longitude)"
    }
    
    @IBAction func buttonPressed(sender: AnyObject) {

        person.firstName = self.firstNameField.text
        person.lastName = lastNameField.text
        person.nationality = nationalityField.text ?? "unknown"
        
        
        labelText.text = "Hi " + person.fullName();
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        //should test to see if the authorization was yes
        locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? HelloViewController {
                controller.person = self.person
        
        }
    }
    

}

