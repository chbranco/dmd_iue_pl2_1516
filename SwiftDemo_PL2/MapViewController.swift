//
//  MapViewController.swift
//  SwiftDemo_PL2
//
//  Created by Catarina Silva on 19/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var map: MKMapView!
    
    let defaults = NSUserDefaults()

    var currentLocation = CLLocationCoordinate2D()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.delegate = self
        map.showsUserLocation = true
        
        let singleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "didTapMap:")
        
        singleTap.delegate = self
        singleTap.numberOfTapsRequired = 1
        //singleTap.numberOfTouchesRequired = 1
        map.addGestureRecognizer(singleTap)
        // Do any additional setup after loading the view.
    }

    func didTapMap(gestureRecognizer: UITapGestureRecognizer) {
        let tapPoint: CGPoint = gestureRecognizer.locationInView(map)
        
        let touchMapCoordinate: CLLocationCoordinate2D = map.convertPoint(tapPoint, toCoordinateFromView: map)
        
        let latDelta:CLLocationDegrees = 10
        let longDelta:CLLocationDegrees = 10
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        
        let region: MKCoordinateRegion = MKCoordinateRegionMake(touchMapCoordinate, span)
        map.setRegion(region, animated: true)
        
        
        let annotation:MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = touchMapCoordinate
        annotation.title = "New double tap location"
        annotation.subtitle = "Where to go next?"
        
        map.addAnnotation(annotation)
        

    }
    
    override func viewDidAppear(animated: Bool) {
        currentLocation.latitude = defaults.doubleForKey("latitude")
        currentLocation.longitude = defaults.doubleForKey("longitude")
        
        let latDelta:CLLocationDegrees = 10
        let longDelta:CLLocationDegrees = 10
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        
        let region: MKCoordinateRegion = MKCoordinateRegionMake(currentLocation, span)
        map.setRegion(region, animated: true)
        
        let annotation:MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = currentLocation
        annotation.title = "Current location"
        annotation.subtitle = "Where to go next?"
        
        map.addAnnotation(annotation)
        
        map.mapType = MKMapType.Satellite
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
