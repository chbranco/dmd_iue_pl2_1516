//
//  HelloViewController.swift
//  SwiftDemo_PL2
//
//  Created by Luis Marcelino on 13/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    
    var person:Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Update name label
        if let p = person {
            nameLabel.text = p.fullName()
        }
        
    }

    @IBAction func changeNameAction(sender: AnyObject) {
        person?.firstName = nameField.text
        nameLabel.text = person?.fullName()
        
        //dismissViewControllerAnimated(true, completion: nil)

    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
